Name:           perl-Date-Manip
Version:        6.95
Release:        2
Summary:        Date manipulation routines
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Date-Manip
Source0:        https://cpan.metacpan.org/authors/id/S/SB/SBECK/Date-Manip-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)  perl(warnings) perl(Carp) perl(Encode) perl(Exporter)
BuildRequires:  perl(integer) perl(IO::File) perl(Storable) perl(Test::Inter)
BuildRequires:  perl(Test::More) perl(utf8)

Requires:       perl(Cwd) perl(File::Find) perl(File::Spec)

Provides:       perl-DateManip = %{version}-%{release}
Obsoletes:      perl-DateManip < 5.48-1

%{?perl_default_filter}

%description
Date::Manip is a series of modules designed to make any common date/time operation easy to do.
Operations such as comparing two times,determining a date a given amount of time from another,
or parsing international times are all easily done. It deals with time as it is used in the
Gregorian calendar (the one currently in use) with full support for time changes due to daylight
saving time.
From the very beginning, the main focus of Date::Manip has been to be able to do ANY desired
date/time operation easily.Many other modules exist which may do a subset of these operations
quicker or more efficiently,but no other module can do all of the operations available in Date::Manip.
Date::Manip has functionality to work with several fundamental types of data.

%package        help
Summary:        Help documents for %{name}
Buildarch:      noarch

%description    help
Man pages and other help documents for %{name}.

%prep
%autosetup -n Date-Manip-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc README.first
%license LICENSE
%{perl_vendorlib}/Date/
%{_bindir}/*

%files help
%doc README
%{_mandir}/man[13]/*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 6.95-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Nov 29 2024 yaoxin <yao_xin001@hoperun.com> - 6.95-1
- Update to 6.95:
  *  Better support for weeks of the year
         Added the Week1ofYear config variable and deprecated the Jan1Week1
         variable. The first week of the year can now be specified much more
         easily.
  *  Support for the standard POSIX strftime directives in printf
         The Date::Manip::Date::printf method uses a set of directives which
         is similar, but not identical, to the POSIX strftime directives.

         A new config variable Use_POSIX_Printf has been added. If set, the
         printf method will use the POSIX strftime directives more fully
         (see the PRINTF DIRECTIVES section in the Date::Manip::Date POD for
         more information).

         Reported in GitHub #48
  *  Time zone fixes
         Newest zoneinfo data (tzdata 2024a).
- License compliance rectification

* Thu Jun 15 2023 wulei <wu_lei@hoperun.com> - 6.92-1
- Update to 6.92

* Wed Jun 22 2022 SimpleUpdate Robot <tc@openeuler.org> - 6.88-1
- Upgrade to version 6.88

* Fri Dec 06 2019 zhouyihang <zhouyihang1@huawei.com> - 6.73-2
- Package init
